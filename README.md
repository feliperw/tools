# DevOps Tools - IAC

## Pré Reqs:
- Instalar o Terraform (https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) na maquina onde será orquestrado o provisionamento da infra
- Ter um usuario programatico na AWS (https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/id_users_create.html)

## Execucão:
- Baixar os arquivos deste repositório:
```
git clone https://gitlab.com/feliperw/tools.git
```

- Acesse o diretório clonado e crie a chave a ser utilizada nas Instâncias da AWS:
```
mkdir keys
ssh-keygen -f keys/aws
```

- Exporte as variáveis de ambiente para acesso a AWS:
```
export AWS_ACCESS_KEY_ID="{Insira o ID do usuário programático da AWS criado nos passos anteriores}"
export AWS_SECRET_ACCESS_KEY="{Insira a Secret do usuário programático da AWS criado nos passos anteriores}"
```

- Execute os comandos do Terraform para criar a infra:
```
terraform init
terraform plan
terraform apply
```

- Após a criação da infra, será exibido o IP de cada VM, endereço de DNS do LB e endpoint do RDS

```
ec2_ip = "X.X.X.X"
```
