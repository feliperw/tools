resource "aws_vpc" "vpc_iac_01" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "vpc-iac-01"
  }
}

resource "aws_subnet" "sn_iac_01" {
  vpc_id     = aws_vpc.vpc_iac_01.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "sn-iac-01"
  }
}

resource "aws_internet_gateway" "ig_iac_01" {
  vpc_id = aws_vpc.vpc_iac_01.id

  tags = {
    Name = "ig-iac-01"
  }
}

resource "aws_route_table" "rt_iac_01" {
  vpc_id = aws_vpc.vpc_iac_01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig_iac_01.id
  }

  tags = {
    Name = "rt-iac-01"
  }
}

resource "aws_route_table_association" "rta_iac_01" {
  subnet_id      = aws_subnet.sn_iac_01.id
  route_table_id = aws_route_table.rt_iac_01.id
}

resource "aws_security_group" "sg_iac_01" {
  name        = "sg_iac_01"
  description = "Allow traffic"
  vpc_id      = aws_vpc.vpc_iac_01.id

  ingress {
    description = "SSH to Instance"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP to Instance"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg-iac-01"
  }
}