resource "aws_key_pair" "key-iac-01" {
  key_name   = "key-iac-01"
  public_key = file("./keys/aws.pub")
}

resource "aws_instance" "vm-iac-01" {
  ami                         = "ami-067d1e60475437da2"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.key-iac-01.key_name
  subnet_id                   = aws_subnet.sn_iac_01.id
  vpc_security_group_ids      = [aws_security_group.sg_iac_01.id]
  associate_public_ip_address = true

  tags = {
    Name = "vm-iac-01"
  }
}

output "ec2_ip" {
  value = aws_instance.vm-iac-01.public_ip
}
